# vim: set fileencoding=utf-8 :

"""
Miscellenious functions not belonging to any specific quantum chemistry program. 
note: visit regex101.com to easily understand and develop regex strings
"""

from datetime import timedelta

# from itertools import pairwise  # New in Python 3.10
from itertools import tee
from typing import List, Dict


def pairwise(iterable):
    """
    itertools.pairwise
    new in Python 3.10
    pairwise('ABCDEFG') --> AB BC CD DE EF FG
    """
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


# def read_cost(*, text: str) -> list[dict[str, str]]:  # New in Python 3.9
def read_cost(*, text: str) -> List[Dict[str, str]]:
    """
    Parse the values of a cost file containing slurm measured data

    :param text: text of the cost file
    :returns: list of dictionaries with columns as keys and text data as values
    for each line in the cost file
    """
    results = []
    lines = text.splitlines()
    columns = lines[0].split()
    col_indices = [0] + [i for i, character in enumerate(lines[1]) if character == " "] + [None]
    for line in lines[2:]:
        if line.strip() == "":
            continue
        result = {}
        for (start, end), column in zip(pairwise(col_indices), columns):
            value = line[start:end].strip()
            if value == "":
                continue
            result[column] = value
        results.append(result)

    return results


def seconds(*, hms: str) -> int:
    """
    Convert the days-hours:minutes:seconds time format to seconds. Days is optional.

    :param hms: text time in days-hours:minutes:seconds formats, e.g. "364-23:59:59"
    :returns: the integer value of total seconds, e.g. 3-23:34:48
    """
    parts = hms.split("-")
    days = 0 if len(parts) == 1 else int(parts[0])
    hours, minutes, seconds = [int(s) for s in parts[-1].split(":")[:3]]
    return int(timedelta(days=days, hours=hours, minutes=minutes, seconds=seconds).total_seconds())


def get_mebi(*, string: str) -> int:
    """
    Convert a string into an integer using the last character as prefix for binary multiples.

    :param string: text with a number and an prefix, e.g. "2048K"
    :return: integer converted to Mi (mebi, 1024*1024) binary multiples, e.g. 2
    :raises ValueError: if the prefix is not in prefixes
    """
    try:
        return int(string)
    except ValueError:
        prefixes = " KMGTPEZY"
        result, unit = string[:-1], string[-1]
        try:
            power = prefixes.index(unit)
        except ValueError as e:
            raise ValueError(f'The unit "{unit}" is not in {prefixes}')
        return round(int(result) * (1024 ** (power - 2)))
