# shell.nix
{ pkgs ? import <nixpkgs> {} }:
let
  my-python = pkgs.python3;
  python-with-my-packages = my-python.withPackages (p: with p; [
    XlsxWriter
  ]);
in with pkgs; 
mkShell {
  buildInputs = [
    black
    git
    less
    neovim
    python-with-my-packages
    xclip
  ];
  shellHook = ''
    PYTHONPATH=${python-with-my-packages}/${python-with-my-packages.sitePackages}
    alias v="nvim"
  '';
}
